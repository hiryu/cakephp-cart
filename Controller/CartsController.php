<?php
Class CartsController extends CartAppController {
	public $uses = ['Cart.OrderItem'];
	public $components = ['Cart.Cart'];

	public function add() {
		if (!$this->request->is('post')) {
			throw new NotFoundException();
		}

		if ($this->Cart->add($this->request->data('OrderItem'))) {
			$this->Flash->success(__('Product added to cart'));
			$this->redirect($this->referer('/'));
		}
	}

	public function delete($item) {
		$this->autoRender = false;
		if ($this->Cart->delete($item)) {
			$this->Flash->success(__('Product removeed to cart'));
			$this->redirect($this->referer('/'));
		}
	}

	public function view() {
		$this->set('cart', $this->Cart->items());
	}

	public function clear() {
		$this->autoRender = false;

		if ($this->Cart->clear()) {
			$this->Flash->success(__('Cart cleared'));
			$this->redirect($this->referer('/'));
		}
	}

}
