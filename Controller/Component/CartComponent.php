<?php
Class CartComponent extends Component {
	public $components = ['Session'];
	private $model = null;

	public function initialize(Controller $controller, $settings = array()) {
		parent::initialize($controller, $settings);
		$this->Controller = $controller;
		$this->Controller->loadModel('Cart.OrderItem');
	}

/**
 * Generate item's hash
 *
 * @return string
 */
	private function _hash($entry) {
		return md5(sprintf('%s.%s', $entry['model'], $entry['foreign_key']));
	}

/**
 * Add an item to cart
 *
 * @return bool
 */
	public function add($items) {
		if ($this->Controller->OrderItem->validates($items)) {

			foreach ($items as $i => $item) {
				$index = $this->_hash($item);

				if ($this->Session->check(sprintf('Cart.items.%s', $index))) {
					$qty = (int) $this->Session->read(sprintf('Cart.items.%s.qty', $index));
					$this->Session->write(sprintf('Cart.items.%s.qty', $index), $qty + (int) $item['qty']);
				} else {
					$this->Session->write(sprintf('Cart.items.%s', $this->_hash($item)), $item);
				}
			}

			return true;
		} else {
			return false;
		}
	}

/**
 * Delete item to cart
 *
 * @return bool
 */
	public function delete($hash) {
		return $this->Session->delete(sprintf('Cart.items.%s', $hash));
	}

/**
 * Clear cart
 *
 * @return bool
 */
	public function clear() {
		return $this->Session->write('Cart.items', []);
	}

/**
 * Show cart items
 * @return array
 */
	public function items() {
		$models  = [];
		$entries = [];

		// auto-import model in controller
		foreach((array) $this->Session->read('Cart.items') as $hash => $item) {
			if (!isset($models[$item['model']])) {
				$this->Controller->loadModel($item['model']);
			}

			$models[$item['model']][] = $item['foreign_key'];
		}

		// reitrive model and foreign_keys from database
		foreach ($models as $modelClass => $ids) {
			if (property_exists($this->Controller->{$modelClass}, 'foreignKey')) {
				$fk = sprintf('%s.%s', $this->Controller->{$modelClass}->alias, $this->Controller->{$modelClass}->foreignKey);
			} else {
				$fk = sprintf('%s.id', $this->Controller->{$modelClass}->alias);
			}

			if (!method_exists($this->Controller->{$modelClass}, 'find')) {
				trigger_error(sprintf('Cannot get Model information: Model %s without find method', $modelClass));
				continue;
			}

			$result = $this->Controller->{$modelClass}->find('all', [
				'conditions' => [
					$fk => $ids
				],
			]);


			$entries[$modelClass] = Set::combine($result, sprintf('{n}.%s.id', $modelClass, $fk), '{n}');
		}

		$items = $this->Session->read('Cart.items');
		foreach ((array) $items as $hash => $item) {

			// Get order name from Session, or from displayField
			if (!isset($item['title'])) {
				$items[$hash]['title']        = $this->Controller->{$modelClass}->displayField;
			}

			if (isset($entries[$item['model']][$item['foreign_key']])) {
				$items[$hash] = array_merge($items[$hash], $entries[$modelClass][$item['foreign_key']]);
			}
		}

		return $items;
	}

/**
 * Check if cart is empty
 *
 * @return bool
 */
	public function isEmpty() {
		$items = $this->Session->read('Cart.items');
		return empty($items);
	}


}
