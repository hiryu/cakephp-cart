<?php
Class Cart extends CartAppModel {
	public $tablePrefix = 'cart_';

	public $hasOne = ['Order'];
	public $hasMany = [
		'OrderItem',
	];

}
