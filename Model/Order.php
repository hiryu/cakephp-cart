<?php
Class Order extends CartAppModel {
	public $tablePrefix = 'cart_';
	public $belongsTo   = [
		'Cart'
	];
}
