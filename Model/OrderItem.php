<?php
Class OrderItem extends CartAppModel {
	public $tablePrefix = 'cart_';
	public $hasOne = ['Order'];
	public $belongsTo = [
		'Cart.Cart',
	];

	public $validationDomain = 'Cart';
	public $validate = [
		/*'cart_id'    => [
			'exists' => [
				//'rule' => 'numeric',
				//'message' => 'Cart id not exists'
			]
		],*/
		'model'      => [
			'check' => [
				'rule'    => ['checkModelExists'],
				'message' => 'Product model not founded'
			]
		],
		'foreign_key' => [
			'check'  => [
				'rule'    => ['checkItemExist'],
				'message' => 'This product not exists',
			]
		],
		'qty'   => [
			'numeric' => [
				'rule'    => ['naturalNumber', false],
				'message' => 'Invalid value'
			],
		],
		'price'      => [
			'numeric' => [
				'rule'    => ['decimal', 2],
				'message' => 'Invalid value'
			],
		]
	];


	private $model = null;
	private $entry = null;

	/** VALIDATIONS **/
	public function checkModelExists($field, $settings = array()) {
		$key   = key($field);
		$value = $field[$key];

		if (strpos('.', $value)) {
			list($plugin, $model) = explode('.', $value);
			$modelClass = $value;
			$prefix     = sprintf('%s.Model', $plugin);
		} else {
			$prefix = 'Model';
			$modelClass = $model  = $value;
		}

		$models = array_flip(App::objects($prefix));
		if (!isset($models[$model])) {
			$this->invalidate($key, __('Model %s not founded', $value));
			return false;
		}

		if (!class_exists($modelClass)) {
			App::uses('Model', $modelClass);
		}

		$this->model = ClassRegistry::init($modelClass);
		return true;
	}

	public function checkItemExist($field, $settings = array()) {
		$key   = key($field);
		$value = $field[$key];

		if (empty($this->data[$this->alias][$key])) {
			$this->invalidate($key, __('Cannot determine product_id from CakeRequest::data'));
			return false;
		}

		$this->entry = $this->model->find('first', ['recursive' => -1, 'conditions' => [ $this->model->primaryKey => $value]]);
		if (empty($this->entry)) {
			$this->invalidate($key, __('This product not exists'));
			return false;
		}

		return true;
	}


}
