<?php $total = 0.00; ?>

<div class="row">
    <div class="col-md-10">

        <table class="table well table-striped table-bordered">
            <thead>
                <tr>
                    <th><?php echo __('Name') ?></th>
                    <th><?php echo __('Qty') ?></th>
                    <th><?php echo __('Price') ?></th>
                    <th><?php echo __('Sub-total') ?></th>
                </tr>
            </thead>
        <?php foreach((array) $cart as $hash => $item) : $total += $item['qty'] * $item['price']; $model = $item['model'] ?>
            <tr>
                <td>
                    <?php
                        echo $this->Html->link(__('Delete'), ['action' => 'delete', $hash], ['class' => 'ui button red']);
                        echo $item['title'];
                    ?>
                </td>
                <td><?php echo $item['qty'] ?></td>
                <td><?php echo $item['price'] ?></td>
                <td><?php echo $item['price'] * $item['qty'] ?></td>
            </tr>
        <?php endforeach ?>
            <tfoot>
                <tr>
                    <td colspan="3" class="text-center"><?php echo __('Total') ?></td>
                    <td><?php echo number_format($total, 2, ',', '.') ?></td>
                </tr>
            </tfoot>
        </table>



    </div>
    <div class="col-md-2">
        <?php
            echo $this->Html->link(__('Clear cart'), ['action' => 'clear'], ['class' => 'ui button large red']);
        ?>
    </div>
</div>
