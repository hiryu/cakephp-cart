<?php
App::uses('Helper', 'View');

Class CartHelper extends Helper {
	public $helpers = ['Form', 'Html', 'Session'];

	private $_settings = [];
	public $settings = [];

/**
 * Constructor
 *
 * @param View $View
 * @param array $settings
 */
	public function __construct(View $View, $settings = array()) {
		$this->settings = array_merge($this->_settings, $settings);
		parent::__construct($View, $settings);
	}

/**
 * URL Without prefixes
 * @param  array $url
 * @param  boolean $full
 * @return str
 */
	public function unprefixed_url($url, $full = false) {
		if (is_array($url)) {
			foreach (Configure::read('Routing.prefixes') as $prefix) {
				$url[$prefix] = false;
			}
		} else {
			trigger_error(__('This method require $url in array format'), E_USER_NOTICE);
		}

		return Router::url($url, $full);
	}

/**
 * URL Without prefixes
 * @param  array $url
 * @param  boolean $full
 * @return str
 */
	public function base_url($url, $full) {
		if (is_array($url)) {
			foreach (Configure::read('Routing.prefixes') as $prefix) {
				$url[$prefix] = false;
			}
		} else {
			trigger_error(__('This method require $url in array format'), E_USER_NOTICE);
		}

		$url['plugin'] = false;
		return Router::url($url, $full);
	}


	public function counter($text = null, $url = null, $attrs = array()) {
		if (!$text) $text = '<i class="icon-shopping-cart">:counter</i>';
		if (!$url) $url  = $this->unprefixed_url(['plugin' => 'cart', 'controller' => 'carts', 'action' => 'view']);

		$args   = ['counter' => sizeof($this->Session->read('Cart.items'))];
		$text   = String::insert($text, $args);
		return $this->Html->link($text, $url, array_merge(['escape' => false], $attrs));
	}

	public function button($product, $formSettings = array(), $buttonSettings = array()) {
		$output = '';
		$btn = ['class' => 'ui button green','label' => __('Add to cart')];
		$frm = ['class' => 'form', 'url'   => $this->unprefixed_url(['plugin' => 'cart', 'controller' => 'carts', 'action' => 'add'])];

		$output .= $this->Form->create('OrderItem', array_merge($frm, $formSettings));
		// Fields
		$output .= $this->Form->hidden('OrderItem.0.foreign_key', ['value' => $product['id'] ]);
		$output .= $this->Form->hidden('OrderItem.0.price', ['value' => $product['price'] ]);
		$output .= $this->Form->input('OrderItem.0.qty', ['type' => 'number', 'min' => 1, 'default' => 1]);
		$output .= $this->Form->hidden('OrderItem.0.model', ['value' => $product['model'] ]);

		$output .= $this->Form->end(array_merge($btn, $buttonSettings));
		return $output;
	}

}
